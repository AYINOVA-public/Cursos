# Desarrollo

## Webpack

* [Guia para principiantes de webpack](https://medium.com/javascript-training/beginner-s-guide-to-webpack-b1f1a3638460)
* [El mejor setup para Webpack](http://www.christianalfoni.com/articles/2015_04_19_The-ultimate-webpack-setup)
* [Webpack y React](http://www.christianalfoni.com/articles/2014_12_13_Webpack-and-react-is-awesome)
* [Setup de webpack con react](https://robots.thoughtbot.com/setting-up-webpack-for-react-and-hot-module-replacement)
* [Super guia de WebPack y React](http://www.pro-react.com/materials/appendixA/)

## Babel

* [Como actualizar Babel](https://medium.com/@malyw/how-to-update-babel-5-x-6-x-d828c230ec53)
* [Stage 0 Babel](https://babeljs.io/docs/en/babel-preset-stage-0/)


 ## Pusher

* [Desarrolla productos con features de tiempo real](https://pusher.com)
* [Features de Pusher](https://pusher.com/features)

## SaaS css

* [build de saas con webpack](https://eng.localytics.com/faster-sass-builds-with-webpack/)

##Tokens y Cookies

* [10 cosas que no sabias de los tokens y cookies](https://auth0.com/blog/ten-things-you-should-know-about-tokens-and-cookies/#token-storage)

 ## Linea de Comandos

* [Conquistando al comando CURL](http://conqueringthecommandline.com/book/curl)
* [Comando de unix y linux para desarrolladores](http://conqueringthecommandline.com/book)


 ## Debug

* [Faster debugging](https://code-cartoons.com/hot-reloading-and-time-travel-debugging-what-are-they-3c8ed2812f35)
* [Enzime, utilidad de test de Javascript](https://medium.com/airbnb-engineering/enzyme-javascript-testing-utilities-for-react-a417e5e5090f)
