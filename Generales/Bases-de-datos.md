# Base de datos

## Mongo DB

* [MongoDB CRUD the MVC way with Passport Authentication](https://hackhands.com/mongodb-crud-mvc-way-with-passport-authentication/)
* [6 Rules of Thumb for MongoDB Schema Design: Part 1](https://www.mongodb.com/blog/post/6-rules-of-thumb-for-mongodb-schema-design-part-1)
* [MongoDB for Node.js Developers curse](https://university.mongodb.com/courses/M101JS/about)
* [Simple MongoDB Instructions Guide](https://www.danielgynn.com/getting-started-mongodb/)

## Mongoose

* [Documentacion de Mongoose, odm para Node.js](http://mongoosejs.com/docs/)
* [Introducing Version 4.0 of the Mongoose NodeJS ODM](https://www.mongodb.com/blog/post/introducing-version-40-mongoose-nodejs-odm)
* [UNDERSTANDING MONGOOSE DEEP POPULATION](http://frontendcollisionblog.com/mongodb/2016/01/24/mongoose-populate.html)


## GraphQL

* [GraphQL: A legit reason to use it.](https://edgecoders.com/graphql-a-legit-reason-to-use-it-7858ce31638a)

## FireBase

* [Using Firebase with ReactJS](https://firebase.googleblog.com/2014/05/using-firebase-with-reactjs.html)
* [Firebase docs](https://www.firebase.com/docs/)


## Lecturas 

* [“Eventual Consistency” vs “Strong Eventual Consistency” vs “Strong Consistency”?](https://stackoverflow.com/questions/29381442/eventual-consistency-vs-strong-eventual-consistency-vs-strong-consistency)
* [SQLite vs MySQL vs PostgreSQL: A Comparison Of Relational Database Management Systems](https://www.digitalocean.com/community/tutorials/sqlite-vs-mysql-vs-postgresql-a-comparison-of-relational-database-management-systems)
* [NoSQL Databases: a Survey and Decision Guidance](https://medium.baqend.com/nosql-databases-a-survey-and-decision-guidance-ea7823a822d)
* [EXPRESSJS WITH AWS EC2 AND DYNAMODB](https://cityos.io/tutorial/1028/1-ExpressJS-with-AWS-EC2-and-DynamoDB)
* [Google Sheets into a Backend Database](https://nobleintentstudio.com/blog/google-docs-as-a-backend/)
* [Designing a RESTful API with Node and Postgres](http://mherman.org/blog/2016/03/13/designing-a-restful-api-with-node-and-postgres/#.WrrsM9Twbcd)




