## Articulos

* 1 [Machine Learning is Fun!](https://medium.com/@ageitgey/machine-learning-is-fun-80ea3ec3c471)

## Gitbooks - Muy Recomendables
* 1 [Intro to machine learning!](https://gueletk.gitbooks.io/intro-to-machine-learning/content/)
* 2 [Artificial Inteligence](https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/chapter1.html)

## EDX 

* 1 [Machine Learning Fundamentals.-Universidad de San Diego](https://courses.edx.org/courses/course-v1:UCSanDiegoX+DS220x+1T2018/course/)
* 2 [Machine Learning -Universidad Georgia](https://courses.edx.org/courses/course-v1:GTx+CS7641+1T2018/course/)
* 3 [Machine Learning -Universidad Columbia](https://courses.edx.org/courses/course-v1:ColumbiaX+CSMM.102x+1T2018/course/)


## UDEMY . 
### Introductorios
* [Deep learning prerequisites the numpy stack in python](https://www.udemy.com/deep-learning-prerequisites-the-numpy-stack-in-python/)


